from django.conf.urls import url
from crawler_core import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
        url(r'^crawls/?$', views.CrawlList.as_view()),
        url(r'^crawl/(?P<pk>[0-9]+)$', views.CrawlDetails.as_view()),
        url(r'^crawl/(?P<pk>[0-9]+)/crawled_sites$',
            views.CrawledSitePerCrawl.as_view()),
        url(r'^crawl/(?P<pk>[0-9]+)/initiate_crawl$',
            views.CrawlInitiateCrawl.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
