from crawler_core.models import CrawledSite, Crawl

from django.test import TestCase as DjangoTestCase


class CrawledSiteAbsolutizeTest(DjangoTestCase):
    def reference_object(self):
        cr = Crawl.objects.create(max_depth=10, base_url='')
        return CrawledSite.objects.create(depth=1,
                                          crawl=cr,
                                          url="https://absolute.com/site")

    def test_other_site_absolute(self):
        other_site = "http://othersite.org"
        cs = self.reference_object()
        self.assertEqual(cs.absolutize(other_site), other_site)

    def test_same_site_absolute(self):
        other_path = "https://absolute.com/hello/world"
        cs = self.reference_object()
        self.assertEqual(cs.absolutize(other_path), other_path)

    def test_relative_path(self):
        relative_path = "/hello/world"
        cs = self.reference_object()
        self.assertEqual(cs.absolutize(relative_path),
                         "https://absolute.com/hello/world")


class CrawledSiteLinksInSiteTest(DjangoTestCase):
    # can I mock classes easily?
    # will it work that well when pulled from DB? probably not..
    class MockCrawledSite(CrawledSite):
        class Meta:
            proxy = True

        def set_docs_list(self, docs_list):
            self.docs_list = docs_list
            self.docs_ind = len(self.docs_list) - 1

        @property
        def html_doc(self):
            self.docs_ind = (self.docs_ind + 1) % len(self.docs_list)
            return self.docs_list[self.docs_ind]

    def reference_object(self, docs_list):
        cr = Crawl.objects.create(max_depth=10, base_url='')
        cs = CrawledSiteLinksInSiteTest.MockCrawledSite.objects.create(
                depth=1,
                crawl=cr,
                url="https://absolute.com/site")
        cs.set_docs_list(docs_list)
        return cs

    def test_empty_site(self):
        docs_list = ['']
        cs = self.reference_object(docs_list)
        self.assertEqual(cs.links_in_site(), [])

    def test_simple_site(self):
        docs_list = ['<a href="/hello">hello</a>']
        cs = self.reference_object(docs_list)
        self.assertEqual(cs.links_in_site(), ["https://absolute.com/hello"])

    def test_two_links(self):
        docs_list = ['<a href="/hello">hello</a><a href="/world">hello</a>']
        cs = self.reference_object(docs_list)
        self.assertEqual(cs.links_in_site(), ["https://absolute.com/hello",
                                              "https://absolute.com/world"])
