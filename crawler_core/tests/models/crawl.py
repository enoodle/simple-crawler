from crawler_core.models import CrawledSite, Crawl

from django.test import TestCase as DjangoTestCase

from urllib.parse import urlparse
import os.path

DEMO_SITE_IN_PROJECT = 'crawler_core/tests/models/demo_site/'


def absolute_path_to_demo_site(page):
    cwd = os.path.abspath(os.path.curdir)
    return 'file://' + os.path.join(cwd, DEMO_SITE_IN_PROJECT, page+'.html')


class CrawlCrawlLayerTest(DjangoTestCase):
    def reference_object(self, url):
        crawl = Crawl.objects.create(max_depth=2, base_url=url)
        crawl.crawledsite_set.add(
                CrawledSite.objects.create(
                    crawl=crawl,
                    depth=0,
                    url=crawl.base_url))
        return crawl

    def test_one_site_layer(self):
        # FIXME the full in project path is not ideal, need relative
        crawl = self.reference_object(
                'file:'+DEMO_SITE_IN_PROJECT+'a.html')
        crawl.crawl_layer(0)
        first_layer = crawl.crawledsite_set.all().filter(depth=1)
        self.assertEqual(len(first_layer), 1)

    def test_two_site_layer(self):
        url = absolute_path_to_demo_site('b')
        crawl = self.reference_object(url)

        crawl.crawl_layer(0)
        self.assertEqual(len(crawl.crawledsite_set.all().filter(depth=1)), 2)
        crawl.crawl_layer(1)
        second_layer = crawl.crawledsite_set.all().filter(depth=2)
        self.assertEqual(len(second_layer), 2)

        self.assertSetEqual(
                {urlparse(x.url).path.split('/')[-1] for x in second_layer},
                {'e.html', 'g.html'})


class CrawlCrawlBFSTest(DjangoTestCase):
    def reference_object(self, url, max_depth=4):
        return Crawl.objects.create(max_depth=max_depth, base_url=url)

    def test_full_bfs_demo_site(self):
        crawl = self.reference_object(absolute_path_to_demo_site('a'))
        crawl.bfs()
        self.assertEqual(len(crawl.crawledsite_set.all()), 8)

    def test_two_bfs_changed_site(self):
        crawl = self.reference_object(absolute_path_to_demo_site('a'), 2)
        crawl.bfs()
        self.assertEqual(len(crawl.crawledsite_set.all()), 4)
        crawl.base_url = absolute_path_to_demo_site('d')
        crawl.save()
        crawl.bfs()
        self.assertSetEqual(
                {urlparse(x.url).path.split('/')[-1] for x in
                    crawl.crawledsite_set.all()},
                {'d.html', 'g.html', 'h.html'})
