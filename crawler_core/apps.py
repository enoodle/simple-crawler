from django.apps import AppConfig


class CrawlerCoreConfig(AppConfig):
    name = 'crawler_core'
