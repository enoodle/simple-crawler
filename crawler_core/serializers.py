from rest_framework import serializers
from .models import Crawl, CrawledSite


class CrawlSerializer(serializers.ModelSerializer):
    """Serializer for Crawl model """
    class Meta:
        model = Crawl
        fields = ('id', 'max_depth', 'base_url')


class CrawledSiteSerializer(serializers.ModelSerializer):
    """Serializer for Crawl model """
    class Meta:
        model = CrawledSite
        fields = ('id', 'crawl', 'depth', 'url')
