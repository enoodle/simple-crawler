from crawler_core.models import Crawl
from crawler_core.serializers import CrawlSerializer, CrawledSiteSerializer
from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.response import Response


class CrawlList(generics.ListCreateAPIView):
    queryset = Crawl.objects.all()
    serializer_class = CrawlSerializer


class CrawlDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Crawl.objects.all()
    serializer_class = CrawlSerializer


class CrawledSitePerCrawl(generics.ListAPIView):
    serializer_class = CrawledSiteSerializer

    def get_queryset(self):
        return Crawl.objects.get(pk=self.kwargs['pk']).crawledsite_set.all()


class CrawlInitiateCrawl(APIView):
    def post(self, request, pk, format=None):
        Crawl.objects.get(pk=pk).bfs()
        return Response(status=status.HTTP_200_OK)
