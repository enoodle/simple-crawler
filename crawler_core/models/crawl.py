from django.db import models
from .crawled_site import CrawledSite


class Crawl(models.Model):
    """
    Crawl is the base crawl representation, starting point and maximum depth.
    Crawled sites will be linked models
    The BFS function is the main crawling logic
    using the CrawledSite method "links_in_site" to get all linked sites
    """
    max_depth = models.IntegerField(default=1)
    base_url = models.CharField(max_length=500)

    def crawl_layer(self, depth):
        for site in self.crawledsite_set.all().filter(depth=depth):
            for link in site.links_in_site():
                # XXX maybe find a better way to test this?
                if len(self.crawledsite_set.all().filter(url=link)) == 0:
                    self.crawledsite_set.add(
                            CrawledSite.objects.create(
                                crawl=self,
                                depth=depth+1,
                                url=link)
                            )

    def bfs(self):
        self.crawledsite_set.all().delete()
        # add zero layer
        self.crawledsite_set.add(
                CrawledSite.objects.create(
                    crawl=self,
                    depth=0,
                    url=self.base_url)
                )

        for depth in range(self.max_depth):
            self.crawl_layer(depth)
