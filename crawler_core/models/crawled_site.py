from django.db import models

from bs4 import BeautifulSoup
import urllib.request
import urllib.parse


class CrawledSite(models.Model):
    """
    CrawledSite represents a site that was visited during the crawl.
    The method links_in_site returns a list of urls to all linked
    pages from this site.
    """
    crawl = models.ForeignKey('Crawl', on_delete=models.CASCADE)
    depth = models.IntegerField(default=0)
    url = models.CharField(max_length=500)

    class Meta:
        ordering = ['depth']

    @property
    def html_doc(self):
        # Returns the html document of the site
        return urllib.request.urlopen(self.url).read()

    def absolutize(self, url):
        return urllib.parse.urljoin(self.url, url)

    def links_in_site(self):
        soup = BeautifulSoup(self.html_doc, 'html.parser')
        return [self.absolutize(a.attrs['href'])
                for a in soup.find_all('a')
                if 'href' in a.attrs]
