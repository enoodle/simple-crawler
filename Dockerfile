FROM python:3.6.3

WORKDIR /usr/src/simple_crawler

COPY requirements.txt ./

RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000

CMD ["./start_container.sh", "0.0.0.0:8000"]
